import React, { Component } from 'react'
import AppHeader from '../common/AppHeader';
import Axios from 'axios';

export default class InvestDetail extends Component {

  constructor(props){
    super(props);
    this.state={
      detail: {}
    }
  }

  componentDidMount(){
    Axios.get(`http://reduxblog.herokuapp.com/api/posts/${this.props.match.params.investId}?key=fwreact`)
    .then(res => {
      this.setState({
        detail : res.data
      })
    })
  }

  render() {
    console.log('ini id', this.props.match.params.investId)
    console.log('ini detail', this.state.detail)
    const { title, categories } = this.state.detail
    return (
      <div>
        <AppHeader />
        <div>
            <h3>{title}</h3>
            <img src={categories} alt="" srcset=""/>
        </div>
      </div>
    )
  }
}
