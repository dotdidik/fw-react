import React, { Component } from 'react'
import {
    Card, CardImg, CardText, CardBody,
    CardTitle, Container, Row, Spinner
} from 'reactstrap';
import Axios from 'axios';

import { Link } from 'react-router-dom';

export default class InvestCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            datas: [],
            isLoading: true
        }
    }

    componentDidMount() {
        Axios.get('http://reduxblog.herokuapp.com/api/posts?key=fwreact')
            .then(res => {
                this.setState({
                    datas: res.data,
                    isLoading: false
                })
            })
    }

    render() {
        let Spin;
        if (this.state.isLoading === true) {
            Spin = <Spinner color="primary" />
        } else {
            Spin = <Container>
                <div>
                    <h2>Content</h2>
                </div>
                <Row>
                    {
                        this.state.datas.map(data =>
                            <Card className='col-md-4'>
                                <Link to={`/investasi/${data.id}`}>
                                    <CardImg className="card-img" top width="100%" src={data.categories} alt="Card image cap" />
                                    <CardBody>
                                        <CardTitle>{data.title}</CardTitle>
                                        <CardText>{data.content}</CardText>
                                    </CardBody>
                                </Link>
                            </Card>
                        )
                    }
                </Row>
            </Container>
        }
        return (
            <div>
                {Spin}
            </div>
        )
    }
}
