import React, { Component } from 'react'

import store from 'store';
import isLoggedIn from '../../helper/is_logged_in'
import { InputGroup, InputGroupAddon, Input, Button } from 'reactstrap';

import { Redirect } from 'react-router-dom'
// import Axios from 'axios';

export default class AuthLogin extends Component {
    constructor(props){
        super(props);
        this.state={
            username: '',
            password: '',
            error: false,
            dataUser: {}
        }
        this.onChangeUsername = this.onChangeUsername.bind(this)
    }

    onChangeUsername(event){
        this.setState({
            username: event.target.value
        });
    }
    onChangePassword(event){
        this.setState({
            password: event.target.value
        });
    }
    onSingInBasic(event){
        event.preventDefault();
        this.setState({
            error: false
        })
        if(!(this.state.username === 'didik' && this.state.password === '123')){
            return this.setState({ error: true })
        }

        store.set('loggedIn', true);
        this.props.history.push('/investasi')
    }
    // onSignIn = event =>{
    //     event.preventDefault();
    //     Axios.post(`http://api-museek.herokuapp.com/auth/login`, 
    //     {
    //         email: "tata@email.com",
    //         password: "tata"
    //     })
    //     .then(response => {
    //         this.setState({
    //           dataUser : response.data
    //         })
    //       })
    // }

        // onSignUp = event =>{
    //     event.preventDefault();
    //     Axios.post(`http://api-museek.herokuapp.com/auth/register`, 
    //     {
    //         email: "tata@email.com",
    //         password: "tata"
    //     })
    //     .then(response => {
    //         this.setState({
    //           dataUser : response.data
    //         })
    //       })
    // }

    render() {
        console.log('ini jwt', isLoggedIn())
        console.log('aakah error?', this.state.error)
        if(isLoggedIn()){
            return <Redirect to='/' />
        }

        return (
            <div>
                <h1>LOGIN</h1>
                <div>
                    <InputGroup>
                        <InputGroupAddon addonType="prepend">@</InputGroupAddon>
                        <Input type="text" value={this.state.username} onChange={this.onChangeUsername} placeholder="username" />
                    </InputGroup>
                    <br />
                    <InputGroup>
                        <InputGroupAddon addonType="prepend">*</InputGroupAddon>
                        <Input type="password" value={this.state.password} onChange={this.onChangePassword.bind(this)} placeholder="password" />
                    </InputGroup>
                </div>
                <Button onClick={this.onSingInBasic.bind(this)} color="info">Cange text</Button>
                <h1>{this.state.username}</h1>
            </div>
        )
    }
}
