import React from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Container,
  Button
} from 'reactstrap';

  import { Link } from 'react-router-dom'

  import store from 'store';
  import isLoggedIn from '../../helper/is_logged_in'

export default class AppHeader extends React.Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  handleLogout = (e) => {
      store.remove('loggedIn');
      this.props.history.push('/login')
  }

  render() {
    console.log('ini', isLoggedIn())
    let isLog;
    if(isLoggedIn()){
      isLog = <NavLink ><Link to='/login' className="text-white">Logout</Link></NavLink>
    } else{
      isLog = <Button onClick={this.handleLogout}>LOGOUT</Button>
    }
    return (
      <div>
        <Navbar color="info" light expand="md">
        <Container>
          <NavbarBrand ><Link to='/' className="text-white">HOMEPAGE</Link></NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse className="" isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink ><Link to='/investasi' className="text-white">Investasi</Link></NavLink>
              </NavItem>
              <NavItem>
                <NavLink className="text-white" href="https://github.com/reactstrap/reactstrap">GitHub</NavLink>
              </NavItem>
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret className="text-white">
                  Options
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    Option 1
                  </DropdownItem>
                  <DropdownItem>
                    Option 2
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem>
                    Reset
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
              <NavItem>
                {isLog}
              </NavItem>
            </Nav>
          </Collapse>
          </Container>
        </Navbar>
      </div>
    );
  }
}